#### _Ichor Studios CRM_

#### By: _**Kayl Eubanks and Jared Lutz**_

## Description

_This webpage application is a customer relationship management system built for Ichor Studios._

## Site viewing for Visitors:
* To view this website, navigate to _______ in a web browser.

## Setup/Installation Requirement for Developers:

* Clone repository on your local computer from https://KaBanks@bitbucket.org/ichorinternship2018/ichor-crm.git.
* If you already have Node, Homebrew, Karma, and Angular installed, then skip to "Install Dependencies."

  #### Node: Windows / Linux Installation Instructions
  * To install Node on other systems, go to the <a href="https://nodejs.org/en/">Node website</a>, download and install the appropriate installer for your operating system.

  #### Node: OSX Installation Instructions
  * On OS X systems, install Node.js through Homebrew with the following command in your home directory:
    * $ brew install node
  * Confirm that node and npm (node package manager, installed automatically with Node) are in place by checking the versions (Node should be 4.0.x or higher, npm should be 3.6.x or higher):
    * $ node -v
    * $ npm -v

  #### Homebrew Installation
  * If you do not have Homebrew installed yet, you may install it by copying and pasting this command:
    * $ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
  * Next, ensure Homebrew packages are run before the system versions of the same (which may be dated or not what we want) by executing the following:
    * $ echo 'export PATH=/usr/local/bin:$PATH' >> ~/.bash_profile

  #### Karma Installation
  * If you do not have Karma installed globally, then run the following:
    * $ npm install -g karma-cli

  #### Angular Installation
  * If you do not have Angular installed globally, then run the following:
    * $ npm install -g @angular/cli@1.6.5

  #### Install dependencies
  * Enter the following code in the command line to install all dependencies:
    * $ npm install

  #### Add Firebase API-key
  * If you do not have a Firebase account:
    * Go to https://console.firebase.google.com/ in a web browser and sign up for one.
  * Create a firebase database for this project.
  * create a file called api-keys.ts using this command:
    * $ ng g /src/app/api-keys.ts
  * open this file in atom and fill with the code below:
      export var masterFirebaseConfig = {
        apiKey: "xxxx",
        authDomain: "xxxx.firebaseapp.com",
        databaseURL: "https://xxxx.firebaseio.com",
        storageBucket: "xxxx.appspot.com",
        messagingSenderId: "xxxx"
      };
  * Replace all instances of 'xxxx' in the previous step with your personal information to connect to the firebase database set up previously.

  #### Working with the project in command line:
  * To build and observe the app on a local host:
    * $ ng serve --open

## Planned Component Tree

![image of component tree](./src/assets/componentTree.jpg)

## Known Bugs

_No known bugs at this time._
_Please contact author at kayleubanks@gmail.com or jaredspecjr@gmail.com with any bugs._

## Technologies Used

| Development dependencies | Front end dependencies |
| :------------ | :------------- |
| * webpack | * bootstrap |
| * eslint | * jquery |
| * karma & jasmine | * popper |
| * babel-loader |  |
| * css-loader & style-loader | |
| * dot-env |  |
| * Angular |  |


### Specs
  * User will be able to sign in using an authentication process
  * User will be able to apply full CRUD functionality to user profiles, client profiles and tracking customer support
  * User will be able to view the following information on client profiles:
    * name of client/company
    * phone number
    * website
    * email
    * start date of relationship
    * special dates or notes
    * important names associated with client
  * Customer support tracking includes information on the following:
    * issue
    * statues of issue
    * date added
    * date completed
    * notes about completion
  * User will be able to receive automated reminder emails about important dates


### License

This software is licensed under the MIT license.

Copyright (c) 2018 ****_Kayl Eubanks and Jared Lutz_****
