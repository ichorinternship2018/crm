import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// import { FormsModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { routing } from './app.routing';
// import { masterFirebaseConfig } from './api-keys';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AppComponent } from './app.component';
import { LandingComponent } from './landing/landing.component';
import { ClientsComponent } from './clients/clients.component';
import { UserProfileComponent } from './user-profile/user-profile.component';

export const firebaseConfig = {
  // apiKey: masterFirebaseConfig.apiKey,
  // authDomain: masterFirebaseConfig.authDomain,
  // databaseURL: masterFirebaseConfig.databaseURL,
  // storageBucket: masterFirebaseConfig.storageBucket
}

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    ClientsComponent,
    UserProfileComponent
  ],
  imports: [
    BrowserModule,
    // FormsModule,
    HttpModule,
    routing,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
