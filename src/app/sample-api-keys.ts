export const masterFirebaseConfig = {
    apiKey: "",
    authDomain: "ichor-crm.firebaseapp.com",
    databaseURL: "https://ichor-crm.firebaseio.com",
    projectId: "ichor-crm",
    storageBucket: "ichor-crm.appspot.com",
    messagingSenderId: "363406088367"
  };

// Add API key and rename file to 'api-keys.ts'
